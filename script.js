function firstFunc() {
    const clients1 = ['Гилберт', 'Сальваторе', 'Пирс', 'Соммерс', 'Форбс', 'Донован', 'Беннет']
    const clients2 = ['Пирс', 'Зальцман', 'Сальваторе', 'Майклсон']
    function bothClients(arr1, arr2) {
        return [...new Set([...arr1, ...arr2])]
    }
    
    console.log(bothClients(clients1, clients2))
}

firstFunc()

//-------------------------------------------------------------------------------------------------------------------------------

function secondFunc() {
    const characters = [
        {
            name: 'Елена',
            lastName: 'Гилберт',
            age: 17,
            gender: 'woman',
            status: 'human',
        },
        {
            name: 'Кэролайн',
            lastName: 'Форбс',
            age: 17,
            gender: 'woman',
            status: 'human',
        },
        {
            name: 'Аларик',
            lastName: 'Зальцман',
            age: 31,
            gender: 'man',
            status: 'human',
        },
        {
            name: 'Дэймон',
            lastName: 'Сальваторе',
            age: 156,
            gender: 'man',
            status: 'vampire',
        },
        {
            name: 'Ребекка',
            lastName: 'Майклсон',
            age: 1089,
            gender: 'woman',
            status: 'vempire',
        },
        {
            name: 'Клаус',
            lastName: 'Майклсон',
            age: 1093,
            gender: 'man',
            status: 'vampire',
        },
    ]
    const charactersShortInfo = []
    characters.forEach((item) => {
        let { name, lastName, age } = item
        charactersShortInfo.push({ name: name, lastName: lastName, age: age })
    })
    console.log(charactersShortInfo)
}

secondFunc()

//-------------------------------------------------------------------------------------------------------------------------------

function thirdFunc() {
    const user1 = {
        name: 'John',
        years: 30,
    }
    const { name, years, isAdmin = false } = user1
    document.body.append(`${name}, ${years}, ${isAdmin}`)
}

thirdFunc()

//-------------------------------------------------------------------------------------------------------------------------------

function fourthFunc() {

    const satoshi2018 = {
        name: 'Satoshi',
        surname: 'Nakamoto',
        technology: 'Bitcoin',
        country: 'Japan',
        browser: 'Tor',
        birth: '1975-04-05',
    }
    const satoshi2019 = {
        name: 'Dorian',
        surname: 'Nakamoto',
        age: 44,
        hidden: true,
        country: 'USA',
        wallet: '1A1zP1eP5QGefi2DMPTfTL5SLmv7DivfNa',
        browser: 'Chrome',
    }
    const satoshi2020 = {
        name: 'Nick',
        surname: 'Sabo',
        age: 51,
        country: 'Japan',
        birth: '1979-08-21',
        location: {
            lat: 38.869422,
            lng: 139.876632,
        },
    }

    const fullProfile = { ...satoshi2018, ...satoshi2019, ...satoshi2020 }
    console.log(fullProfile)
}

fourthFunc()

//-------------------------------------------------------------------------------------------------------------------------------

function fifthFunc() {

    const books = [
        {
            name: 'Harry Potter',
            author: 'J.K. Rowling',
        },
        {
            name: 'Lord of the rings',
            author: 'J.R.R. Tolkien',
        },
        {
            name: 'The witcher',
            author: 'Andrzej Sapkowski',
        },
    ]
    const bookToAdd = {
        name: 'Game of thrones',
        author: 'George R. R. Martin',
    }

    const addBooks = [...books, bookToAdd]
    console.log(addBooks)
}

fifthFunc()

//-------------------------------------------------------------------------------------------------------------------------------

function sixthFunc() {
    const employee = {
        name: 'Vitalii',
        surname: 'Klichko',
    }
    const allAboutVitalj = { ...employee, age: 43, salary: 491666667 }
    console.log(allAboutVitalj)
}

sixthFunc()

//-------------------------------------------------------------------------------------------------------------------------------

function seventhFunc() {
    const array = ['value', () => 'showValue']
    const [value, showValue] = array
    console.log(value)
    console.log(showValue())
}

seventhFunc()